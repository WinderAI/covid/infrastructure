# infrastructure

Infrastructure for the Athena Epidemic Project

## Get the kubeconfig file

```
gcloud init
```

```
gcloud container clusters get-credentials prod --region europe-west1-d --project athena-epidemic
```


## Initial Setup (Should not be required)

```
glcoud init
gcloud auth application-default login
```

```
gcloud services enable storage-api.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable compute.googleapis.com
gcloud services enable container.googleapis.com
gcloud services enable iam.googleapis.com
```

```
export PROJECT_ID=athena-epidemic
gcloud iam service-accounts create terraform
gcloud projects add-iam-policy-binding ${PROJECT_ID} --member "serviceAccount:terraform@${PROJECT_ID}.iam.gserviceaccount.com" --role "roles/owner"
gcloud iam service-accounts keys create key.json --iam-account terraform@${PROJECT_ID}.iam.gserviceaccount.com
export GOOGLE_APPLICATION_CREDENTIALS="$PWD/key.json"
```

```
export REGION=europe-west1
export BUCKET_NAME=athena-epidemic-tfstate
gsutil mb -l ${REGION} gs://${BUCKET_NAME}
```

```
terraform init
terraform validate
```

```
terraform apply
```

```
gcloud container clusters get-credentials prod --region europe-west1-d --project athena-epidemic
kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
kubectl apply -f gitlab-admin-service-account.yaml
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
```

## Next Steps

Now go to the base repository to install the basic K8s requirements for Gitlab/DNS.