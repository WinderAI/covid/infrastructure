image:
  name: hashicorp/terraform:light
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

# Default output file for Terraform plan
variables:
  PLAN: plan.tfplan
  TF_IN_AUTOMATION: "true"

cache:
  key: "$CI_COMMIT_REF_SLUG"
  paths:
    - .terraform

before_script:
  - apk add --update curl
  - curl -o kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/kubectl
  - install kubectl /usr/local/bin/ && rm kubectl
  - curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
  - echo $SERVICE_ACCOUNT | base64 -d > ./creds/serviceaccount.json
  - terraform --version
  - terraform init

stages:
  - validate
  - plan
  - apply
  - deploy
  - destroy

validate:
  stage: validate
  script:
    - terraform validate
    - terraform fmt -check=true
  only:
    - branches

merge review:
  stage: plan
  script:
    - terraform plan -out=$PLAN
    - echo \`\`\`diff > plan.txt
    - terraform show -no-color ${PLAN} | tee -a plan.txt
    - echo \`\`\` >> plan.txt
    - sed -i -e 's/  +/+/g' plan.txt
    - sed -i -e 's/  ~/~/g' plan.txt
    - sed -i -e 's/  -/-/g' plan.txt
    - MESSAGE=$(cat plan.txt)
    - >-
      curl -X POST -g -H "PRIVATE-TOKEN: ${GITLAB_TOKEN}" 
      --data-urlencode "body=${MESSAGE}" 
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/discussions"
  artifacts:
    name: plan
    paths:
      - $PLAN
  only:
    - merge_requests

plan production:
  stage: plan
  script:
    - terraform plan -out=$PLAN
  artifacts:
    name: plan
    paths:
      - $PLAN
  only:
    - master
  resource_group: production

apply:
  stage: apply
  script:
    - terraform apply -input=false $PLAN
    - helm repo update
    - helm install nginx-ingress stable/nginx-ingress --set rbac.create=true --set controller.publishService.enabled=true --set controller.metrics.enabled=true
  dependencies:
    - plan production
  artifacts:
    name: $CI_COMMIT_REF_SLUG
    untracked: true
  only:
    - master
  resource_group: production
  environment:
    name: production
    url: $DYNAMIC_ENVIRONMENT_URL
    on_stop: destroy

# deploy-apps:
#   stage: deploy
#   trigger: gitops-demo/apps/cluster-management

destroy:
  stage: destroy
  script:
    - terraform destroy -auto-approve
  when: manual
  only:
    - master  
  environment:
    name: production
    action: stop
