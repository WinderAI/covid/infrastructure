gcp_project_id = "athena-epidemic"
gcp_region     = "europe-west1"
gcp_zones      = ["europe-west1-d"]

cluster_name                        = "prod"
daily_maintenance_window_start_time = "03:00"
node_pools = [
  {
    name            = "default"
    machine_type    = "e2-medium"
    min_count       = 1 # Nodes per zone! Must be at least 1 to host system pods
    max_count       = 1
    local_ssd_count = 0
    disk_size_gb    = 30
    disk_type       = "pd-standard"
    image_type      = "COS"
    auto_repair     = true
    preemptible     = true
  }
]

vpc_network_name = "vpc-network"

vpc_subnetwork_name = "vpc-subnetwork"

vpc_subnetwork_cidr_range = "10.0.16.0/20"

cluster_secondary_range_name = "pods"

cluster_secondary_range_cidr = "10.16.0.0/12"

services_secondary_range_name = "services"

services_secondary_range_cidr = "10.1.0.0/20"

master_ipv4_cidr_block = "172.16.0.0/28"

access_private_images = "false"

http_load_balancing_disabled = "false"

master_authorized_networks_cidr_blocks = [
  {
    cidr_block = "0.0.0.0/0"

    display_name = "default"
  },
]

network_policy_enabled = false

client_certificate_enabled = true
